const mongoose= require('mongoose');

const schema= mongoose.Schema;

const postSchema= new schema({
    userId:
        {
        type:mongoose.Types.ObjectId,
        ref:"user"
        },
    __id:{
        type:mongoose.Types.ObjectId,

    },
    title:{
        type:String,
        require:true
    },
    body:{
        type:String,
        required:true
    }
})
module.exports=mongoose.model("post",postSchema);