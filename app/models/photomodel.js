const mongoose=require('mongoose');

const schema= mongoose.Schema;

const newPhotoSchema= new schema({
    abumId:{
        type:mongoose.Types.ObjectId,
        ref:"abum"
    },
    __id:{
        type:mongoose.Types.ObjectId
    },
    title:{
        type:String,
        required:true
    },
    url:{
        type:String,
        required:true
    },
    thumbnaiUrl:{
        type:String,
        required:true
    }
},
{
    timestamps:true
})
module.exports=mongoose.model("photo",newPhotoSchema);