const mongoose=require('mongoose');

const schema=mongoose.Schema;

const commentSchema=new schema({
    postId:{
        type:mongoose.Types.ObjectId,
        ref:"post"
    },
    __id:{
        type:mongoose.Types.ObjectId
    },
    name:{
        type:String,
        required:true
    },
    email:{
        type:String,
        required:true
    },
    body:{
        type:String,
        required:true
    }
})
module.exports=mongoose.model("comment",commentSchema);