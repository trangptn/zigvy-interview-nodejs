const mongoose= require('mongoose');

const schema= mongoose.Schema;

const newAbumSchema= new schema({
    userId:{
        type:mongoose.Types.ObjectId,
        ref:"user",
        require:true
    },
    __id:{
        type:mongoose.Types.ObjectId
    },
    title:{
        type:String,
        required:true
    },
},
{
    timestamps:true
})
module.exports=mongoose.model("abum",newAbumSchema);