const mongoose=require('mongoose');

const schema= mongoose.Schema;

const newTodoSchema= new schema({
    userId:{
        type:mongoose.Types.ObjectId,
        ref:"user"
    },
    __id:mongoose.Types.ObjectId,
    title:{
        type:String,
        required:true
    },
    completed:{
        type:Boolean,
        required:true
    }
},
{
    timestamps:true
})
module.exports=mongoose.model("todo",newTodoSchema);