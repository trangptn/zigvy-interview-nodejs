const mongoose = require('mongoose');

const postModel = require("../models/postModel");


const createPost = async (req, res) => {
    //Thu thap du lieu
    const {
        reqUserId,
        reqTitle,
        reqBody
    } = req.body

    //validate du lieu
    if (!mongoose.Types.ObjectId.isValid(reqUserId)) {
        res.status(400).json({
            message: "User Id khong hop le"
        })
        return false;
    }
    if (!reqTitle) {
        res.status(400).json({
            message: "Title khong hop le"
        })
        return false;
    }
    if (!reqBody) {
        res.status(400).json({
            message: "Body khong hop le"
        })
        return false;
    }
    try {
        //Xu ly du lieu
        const newPost = {
            userId: reqUserId,
            title: reqTitle,
            body: reqBody
        }
        const result = await postModel.create(newPost);
        res.status(201).json({
            message: "Tao moi thanh cong",
            data: result
        })
    } catch (err) {
        console.log(err);
        res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}

const getAllPost = async (req, res) => {
    const userId=req.query.userId;
    const condition={};
    if(mongoose.Types.ObjectId.isValid(userId))
    {
        condition.userId=userId;
    }
    try {
        const result = await postModel.find(condition);
        res.status(200).json({
            message: "Lay du lieu thanh cong",
            data: result
        })
    }
    catch (err) {
        res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}

const getPostById = async (req, res) => {
    const postid = req.params.postid;
    if (!mongoose.Types.ObjectId.isValid(postid)) {
        res.status(400).json({
            message: "Post Id khong hop le"
        })
        return false;
    }
    const result = await postModel.findById(postid);
    try {
        res.status(200).json({
            message: "Lay du lieu thanh cong",
            data: result
        })
    }
    catch (err) {
        res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}

const updatePostById = async (req, res) => {
    const postid = req.params.postid;
    const {
        reqUserId,
        reqTitle,
        reqBody
    } = req.body
    //validate du lieu
    if (!mongoose.Types.ObjectId.isValid(postid)) {
        res.status(400).json({
            message: "Ma post id khong hop le"
        })
        return false;
    }
    if (reqTitle === "") {
        res.status(400).json({
            message: "Title khong hop le"
        })
        return false;
    }
    if (reqBody === "") {
        res.status(400).json({
            message: "Body khong hop le"
        })
        return false;
    }
    try {
        var newPost = {
        };
        if (reqUserId) {
            newPost.userId = reqUserId;
        }
        if (reqTitle) {
            newPost.title = reqTitle;
        }
        if (reqBody) {
            newPost.body = reqBody;
        }

        const result = await postModel.findByIdAndUpdate(postid, newPost);
        if (result) {
            res.status(200).json({
                message: "Cap nhat thong tin thanh cong",
                data: result
            })
        }
        else {
            res.status(400).json({
                message: "Khong tim thay thong tin post",
            })
        }
    } catch (err) {
        console.log(err);
        res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}

const deletePostById = async (req, res) => {
    const postid = req.params.postid;
    if (!mongoose.Types.ObjectId.isValid(postid)) {
        res.status(400).json({
            message: "Ma post id khong hop le"
        })
        return false;
    }

    try {
        const result = await postModel.findByIdAndDelete(postid);
        if (result) {
            res.status(200).json({
                message: "Xoa thong tin thanh cong",
            })
        }
        else {
            res.status(400).json({
                message: "Khong tim thay thong tin post",
            })
        }
    } catch (err) {
        console.log(err);
        res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}

const getPostsOfUser = async (req,res)=>{
    const userId=req.params.userId;
    const condition={};
    if(mongoose.Types.ObjectId.isValid(userId))
    {
        condition.userId=userId;
    }
    console.log(userId);
    console.log(condition);
    const result=await postModel.find(condition);
    try{
        res.status(200).json({
            message:"Lay du lieu thanh cong",
            data:result
        })
    }
    catch(err)
    {
        res.status.json({
            message:"Co loi xay ra"
        })
    }
}
module.exports = {
    createPost,
    getAllPost,
    getPostById,
    updatePostById,
    deletePostById,
    getPostsOfUser
}
