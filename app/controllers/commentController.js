const mongoose = require('mongoose');

const commentModel = require("../models/commentModel");

const createComment = async (req, res) => {
    //Thu thap du lieu
    const {
        reqPostId,
        reqName,
        reqEmail,
        reqBody
    } = req.body

    //validate du lieu
    if (!mongoose.Types.ObjectId.isValid(reqPostId)) {
        res.status(400).json({
            message: "Post Id khong hop le"
        })
        return false;
    }
    if (!reqName) {
        res.status(400).json({
            message: "Name khong hop le"
        })
        return false;
    }
    if (!reqEmail) {
        res.status(400).json({
            message: "Email khong hop le"
        })
        return false;
    }
    if (!reqBody) {
        res.status(400).json({
            message: "Body khong hop le"
        })
        return false;
    }
    try {
        //Xu ly du lieu
        const newComment = {
            postId: reqPostId,
            name: reqName,
            email:reqEmail,
            body: reqBody
        }
        const result = await commentModel.create(newComment);
        res.status(201).json({
            message: "Tao moi thanh cong",
            data: result
        })
    } catch (err) {
        console.log(err);
        res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}

const getAllComment= async (req, res) => {
    const postId=req.query.postId;
    const condition={};
    if(mongoose.Types.ObjectId.isValid(postId))
    {
        condition.postId=postId;
    }

    try {
        const result = await commentModel.find(condition);
        res.status(200).json({
            message: "Lay du lieu thanh cong",
            data: result
        })
    }
    catch (err) {
        res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}

const getComentById = async (req, res) => {
    const commentid = req.params.commentid;
    if (!mongoose.Types.ObjectId.isValid(commentid)) {
        res.status(400).json({
            message: "Comment Id khong hop le"
        })
        return false;
    }
    const result = await commentModel.findById(commentid);
    try {
        res.status(200).json({
            message: "Lay du lieu thanh cong",
            data: result
        })
    }
    catch (err) {
        res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}

const updateCommentById = async (req, res) => {
    const commentid = req.params.commentid;
    const {
        reqPostId,
        reqName,
        reqEmail,
        reqBody
    } = req.body
    //validate du lieu
    if (!mongoose.Types.ObjectId.isValid(commentid)) {
        res.status(400).json({
            message: "Ma comment id khong hop le"
        })
        return false;
    }
    if (reqName === "") {
        res.status(400).json({
            message: "Title khong hop le"
        })
        return false;
    }
    if (reqEmail === "") {
        res.status(400).json({
            message: "Email khong hop le"
        })
        return false;
    }
    if (reqBody === "") {
        res.status(400).json({
            message: "Body khong hop le"
        })
        return false;
    }
    try {
        var newComment = {
        };

        if (reqName) {
            newComment.name = reqName;
        }
        if (reqEmail) {
            newPost.email = reqEmail;
        }
        if (reqBody) {
            newComment.body = reqBody;
        }

        const result = await commentModel.findByIdAndUpdate(commentid, newComment);
        if (result) {
            res.status(200).json({
                message: "Cap nhat thong tin thanh cong",
                data: result
            })
        }
        else {
            res.status(400).json({
                message: "Khong tim thay thong tin comment",
            })
        }
    } catch (err) {
        console.log(err);
        res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}

const deleteCommentById = async (req, res) => {
    const commentid = req.params.commentid;
    if (!mongoose.Types.ObjectId.isValid(commentid)) {
        res.status(400).json({
            message: "Ma comment id khong hop le"
        })
        return false;
    }

    try {
        const result = await commentModel.findByIdAndDelete(commentid);
        if (result) {
            res.status(200).json({
                message: "Xoa thong tin thanh cong",
            })
        }
        else {
            res.status(400).json({
                message: "Khong tim thay thong tin comment",
            })
        }
    } catch (err) {
        console.log(err);
        res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}

const getCommentsOfPost=async (req,res)=>{
    const postId=req.params.postId;
    const condition={};
    if(mongoose.Types.ObjectId.isValid(postId))
    {
        condition.postId=postId;
    }
   
    try{
        const result=await commentModel.find(condition);
        res.status(200).json({
            message:"Lay thong tin thanh cong",
            data:result
        })
    }
    catch(err)
    {
        res.status(500).json({
            message:"Co loi xay ra"
        })
    }
}
module.exports = {
    createComment,
    getAllComment,
    getComentById,
    updateCommentById,
    deleteCommentById,
    getCommentsOfPost
}
