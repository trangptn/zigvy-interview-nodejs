const mongoose = require('mongoose');

const todoModel = require("../models/todoModel");

const createTodo = async (req, res) => {
    //Thu thap du lieu
    const {
        reqUserId,
        reqTitle,
        reqCompleted
    } = req.body

    //validate du lieu
    if (!mongoose.Types.ObjectId.isValid(reqUserId)) {
        res.status(400).json({
            message: "User Id khong hop le"
        })
        return false;
    }
    if (!reqTitle) {
        res.status(400).json({
            message: "Title khong hop le"
        })
        return false;
    }
    if (!reqCompleted) {
        res.status(400).json({
            message: "Completed khong hop le"
        })
        return false;
    }
    try {
        //Xu ly du lieu
        const newTodo = {
            title: reqTitle,
            userId: reqUserId,
            completed:reqCompleted
        }
        const result = await todoModel.create(newTodo);
        res.status(201).json({
            message: "Tao moi thanh cong",
            data: result
        })
    } catch (err) {
        console.log(err);
        res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}

const getAllTodo= async (req, res) => {
    const userId=req.query.userId;
    const condition={};
    if(mongoose.Types.ObjectId.isValid(userId))
    {
        condition.userId=userId;
    }

    try {
        const result = await todoModel.find(condition);
        res.status(200).json({
            message: "Lay du lieu thanh cong",
            data: result
        })
    }
    catch (err) {
        res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}

const getTodoById = async (req, res) => {
    const todoid = req.params.todoid;
    if (!mongoose.Types.ObjectId.isValid(todoid)) {
        res.status(400).json({
            message: "Todo Id khong hop le"
        })
        return false;
    }
    const result = await todoModel.findById(todoid);
    try {
        res.status(200).json({
            message: "Lay du lieu thanh cong",
            data: result
        })
    }
    catch (err) {
        res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}

const updateTodoById = async (req, res) => {
    const todoid = req.params.todoid;
    const {
        reqTitle,
        reqCompleted,
    } = req.body
    //validate du lieu
    if (!mongoose.Types.ObjectId.isValid(todoid)) {
        res.status(400).json({
            message: "Ma todo id khong hop le"
        })
        return false;
    }
    if (reqTitle === "") {
        res.status(400).json({
            message: "Title khong hop le"
        })
        return false;
    }
    if (reqCompleted === "") {
        res.status(400).json({
            message: "completed khong hop le"
        })
        return false;
    }
    try {
        var newTodo= {};

        if (reqTitle) {
            newTodo.title = reqTitle;
        }
        if (reqCompleted) {
            newTodo.completed = reqCompleted;
        }

        const result = await todoModel.findByIdAndUpdate(todoid, newTodo);
        if (result) {
            res.status(200).json({
                message: "Cap nhat thong tin thanh cong",
                data: result
            })
        }
        else {
            res.status(400).json({
                message: "Khong tim thay thong tin todo",
            })
        }
    } catch (err) {
        console.log(err);
        res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}

const deleteTodoById = async (req, res) => {
    const todoid = req.params.todoid;
    if (!mongoose.Types.ObjectId.isValid(todoid)) {
        res.status(400).json({
            message: "Ma todo id khong hop le"
        })
        return false;
    }

    try {
        const result = await todoModel.findByIdAndDelete(todoid);
        if (result) {
            res.status(200).json({
                message: "Xoa thong tin thanh cong",
            })
        }
        else {
            res.status(400).json({
                message: "Khong tim thay thong tin todo",
            })
        }
    } catch (err) {
        console.log(err);
        res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}
const getTodosOfUser= async (req,res)=>{
    const userId=req.params.userId;
    const condition={};
    if(mongoose.Types.ObjectId.isValid(userId))
    {
        condition.userId=userId;
    }
    try{
        const result=await todoModel.find(condition);
        res.status(200).json({
            message:"Lay thong tin thanh cong",
            data:result
        })
    }
    catch(err)
    {
        res.status(500).json({
            message:"Co loi xay ra"
        })
    }
}
module.exports = {
    createTodo,
    getAllTodo,
    getTodoById,
    updateTodoById,
    deleteTodoById,
    getTodosOfUser
}
