const mongoose = require('mongoose');

const userModel = require("../models/userModel");

const createUser = async (req, res) => {
    //Thu thap du lieu
    const {
        reqName,
        reqUserName,
        reqEmail,
        reqStreet,
        reqSuite,
        reqCity,
        reqZipCode,
        reqLat,
        reqLng,
        reqPhone,
        reqWebsite,
        reqNameCompany,
        reqCatchPhrase,
        reqBs
    } = req.body

    //validate du lieu
    if (!reqName) {
        res.status(400).json({
            message: "Name khong hop le"
        })
        return false;
    }
    if (!reqUserName) {
        res.status(400).json({
            message: "Username khong hop le"
        })
        return false;
    }
    if (!reqStreet) {
        res.status(400).json({
            message: "Street khong hop le"
        })
        return false;
    }
    if (!reqSuite) {
        res.status(400).json({
            message: "Suite khong hop le"
        })
        return false;
    }
    if (!reqCity) {
        res.status(400).json({
            message: "City khong hop le"
        })
        return false;
    }
    if (!reqZipCode) {
        res.status(400).json({
            message: "ZipCode khong hop le"
        })
        return false;
    }
    if (!reqLat) {
        res.status(400).json({
            message: "Lat khong hop le"
        })
        return false;
    }
    if (!reqLng) {
        res.status(400).json({
            message: "Lng khong hop le"
        })
        return false;
    }
    if (!reqPhone) {
        res.status(400).json({
            message: "Phone khong hop le"
        })
        return false;
    }
    var emailReg = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
    if (!reqEmail || !emailReg.test(reqEmail)) {
        res.status(400).json({
            message: "Email khong hop le"
        })
        return false;
    }
    if (!reqWebsite) {
        res.status(400).json({
            message: "Website khong hop le"
        })
        return false;
    }
    if (!reqNameCompany) {
        res.status(400).json({
            message: "Name company khong hop le"
        })
        return false;
    }
    if (!reqCatchPhrase) {
        res.status(400).json({
            message: "Catch Phrase khong hop le"
        })
        return false;
    }
    if (!reqBs) {
        res.status(400).json({
            message: "Bs khong hop le"
        })
        return false;
    }
    try {
        //Xu ly du lieu
        const newUser = {
            name: reqName,
            username: reqUserName,
            email: reqEmail,
            address: {
                street: reqStreet,
                suite: reqSuite,
                city: reqCity,
                zipcode: reqZipCode,
                geo: {
                    lat: reqLat,
                    lng: reqLng
                }
            },
            phone: reqPhone,
            website: reqWebsite,
            company: {
                name: reqNameCompany,
                catchPhrase: reqCatchPhrase,
                bs: reqBs
            }
        }
        const result = await userModel.create(newUser);
        res.status(201).json({
            message: "Tao moi thanh cong",
            data: result
        })
    } catch (err) {
        console.log(err);
        res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}

const getAllUser = async (req, res) => {
    try {
        const result = await userModel.find();
        res.status(200).json({
            message: "Lay du lieu thanh cong",
            data: result
        })
    }
    catch (err) {
        res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}

const getUserById = async (req, res) => {
    const userid = req.params.userid;
    if (!mongoose.Types.ObjectId.isValid(userid)) {
        res.status(400).json({
            message: "User Id khong hop le"
        })
        return false;
    }
    const result = await userModel.findById(userid);
    try {
        res.status(200).json({
            message: "Lay du lieu thanh cong",
            data: result
        })
    }
    catch (err) {
        res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}

const updateUserById = async (req, res) => {
    const userid = req.params.userid;
    const {
        reqName,
        reqUserName,
        reqEmail,
        reqStreet,
        reqSuite,
        reqCity,
        reqZipCode,
        reqLat,
        reqLng,
        reqPhone,
        reqWebsite,
        reqNameCompany,
        reqCatchPhrase,
        reqBs
    } = req.body
    //validate du lieu
    if (!mongoose.Types.ObjectId.isValid(userid)) {
        res.status(400).json({
            message: "Ma user id khong hop le"
        })
        return false;
    }
    if (reqName==="") {
        res.status(400).json({
            message: "Name khong hop le"
        })
        return false;
    }
    if (reqUserName==="") {
        res.status(400).json({
            message: "Username khong hop le"
        })
        return false;
    }
    if (reqStreet==="") {
        res.status(400).json({
            message: "Street khong hop le"
        })
        return false;
    }
    if (reqSuite==="") {
        res.status(400).json({
            message: "Suite khong hop le"
        })
        return false;
    }
    if (reqCity==="") {
        res.status(400).json({
            message: "City khong hop le"
        })
        return false;
    }
    if (reqZipCode==="") {
        res.status(400).json({
            message: "ZipCode khong hop le"
        })
        return false;
    }
    if (reqLat==="") {
        res.status(400).json({
            message: "Lat khong hop le"
        })
        return false;
    }
    if (reqLng==="") {
        res.status(400).json({
            message: "Lng khong hop le"
        })
        return false;
    }
    if (reqPhone==="") {
        res.status(400).json({
            message: "Phone khong hop le"
        })
        return false;
    }
    if (reqEmail==="") {
        res.status(400).json({
            message: "Email khong hop le"
        })
        return false;
    }
    if (reqWebsite==="") {
        res.status(400).json({
            message: "Website khong hop le"
        })
        return false;
    }
    if (reqNameCompany==="") {
        res.status(400).json({
            message: "Name company khong hop le"
        })
        return false;
    }
    if (reqCatchPhrase==="") {
        res.status(400).json({
            message: "Catch Phrase khong hop le"
        })
        return false;
    }
    if (reqBs==="") {
        res.status(400).json({
            message: "Bs khong hop le"
        })
        return false;
    }
    try {
        var newUser = {
        };
        if (reqName) {
            newUser.name = reqName;
        }
        if(reqUserName)
        {
            newUser.username=reqUserName;
        }
        if(reqPhone)
        {
            newUser.phone=reqPhone;
        }
        if(reqWebsite)
        {
            newUser.website=reqWebsite;
        }
        const result=await userModel.findByIdAndUpdate(userid,newUser);
        if(result)
        {
            res.status(200).json({
                message:"Cap nhat thong tin thanh cong",
                data:result
            })
        }
        else{
            res.status(400).json({
                message:"Khong tim thay thong tin user",
            })
        }
    } catch (err) {
        console.log(err);
        res.status(500).json({
            message:"Co loi xay ra"
        })
    }
}

const deleteUserById= async (req,res) =>{
    const userid=req.params.userid;
    if (!mongoose.Types.ObjectId.isValid(userid)) {
        res.status(400).json({
            message: "Ma user id khong hop le"
        })
        return false;
    }

    try{
        const result=await userModel.findByIdAndDelete(userid);
        if(result)
        {
            res.status(200).json({
                message:"Xoa thong tin thanh cong",
            })
        }
        else{
            res.status(400).json({
                message:"Khong tim thay thong tin user",
            })
        }
    } catch (err) {
        console.log(err);
        res.status(500).json({
            message:"Co loi xay ra"
        })
    }
}
    module.exports = {
        createUser,
        getAllUser,
        getUserById,
        updateUserById,
        deleteUserById
    }
