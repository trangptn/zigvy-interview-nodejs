const mongoose = require('mongoose');

const photoModel = require("../models/photomodel");

const createPhoto = async (req, res) => {
    //Thu thap du lieu
    const {
        reqAlbumId,
        reqTitle,
        reqUrl,
        reqThumbnailUrl
    } = req.body

    //validate du lieu
    if (!mongoose.Types.ObjectId.isValid(reqAlbumId)) {
        res.status(400).json({
            message: "Album Id khong hop le"
        })
        return false;
    }
    if (!reqTitle) {
        res.status(400).json({
            message: "Title khong hop le"
        })
        return false;
    }
    if (!reqUrl) {
        res.status(400).json({
            message: "Url khong hop le"
        })
        return false;
    }
    if (!reqThumbnailUrl) {
        res.status(400).json({
            message: "Thumbnail Url khong hop le"
        })
        return false;
    }
    try {
        //Xu ly du lieu
        const newPhoto = {
            title: reqTitle,
            abumId: reqAlbumId,
            url:reqUrl,
            thumbnaiUrl:reqThumbnailUrl
        }
        const result = await photoModel.create(newPhoto);
        res.status(201).json({
            message: "Tao moi thanh cong",
            data: result
        })
    } catch (err) {
        console.log(err);
        res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}

const getAllPhoto= async (req, res) => {
    const abumId=req.query.abumId;
    const condition={};
    if(mongoose.Types.ObjectId.isValid(abumId))
    {
        condition.abumId=abumId;
    }

    try {
        const result = await photoModel.find(condition);
        res.status(200).json({
            message: "Lay du lieu thanh cong",
            data: result
        })
    }
    catch (err) {
        res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}

const getPhotoById = async (req, res) => {
    const photoid = req.params.photoid;
    if (!mongoose.Types.ObjectId.isValid(photoid)) {
        res.status(400).json({
            message: "Photo Id khong hop le"
        })
        return false;
    }
    const result = await photoModel.findById(photoid);
    try {
        res.status(200).json({
            message: "Lay du lieu thanh cong",
            data: result
        })
    }
    catch (err) {
        res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}

const updatePhotoById = async (req, res) => {
    const photoid = req.params.photoid;
    const {
        reqUrl,
        reqThumbnailUrl,
        reqTitle,
    } = req.body
    //validate du lieu
    if (!mongoose.Types.ObjectId.isValid(photoid)) {
        res.status(400).json({
            message: "Ma photo id khong hop le"
        })
        return false;
    }
    if (reqTitle === "") {
        res.status(400).json({
            message: "Title khong hop le"
        })
        return false;
    }
    if (reqUrl === "") {
        res.status(400).json({
            message: "Url khong hop le"
        })
        return false;
    }
    if (reqThumbnailUrl === "") {
        res.status(400).json({
            message: "Thumbnail Url khong hop le"
        })
        return false;
    }
    try {
        var newPhoto= {
        };

        if (reqTitle) {
            newPhoto.title = reqTitle;
        }
        if (reqUrl) {
            newPhoto.url = reqUrl;
        }
        if (reqThumbnailUrl) {
            newPhoto.thumbnaiUrl = reqThumbnailUrl;
        }

        const result = await photoModel.findByIdAndUpdate(photoid, newPhoto);
        if (result) {
            res.status(200).json({
                message: "Cap nhat thong tin thanh cong",
                data: result
            })
        }
        else {
            res.status(400).json({
                message: "Khong tim thay thong tin photo",
            })
        }
    } catch (err) {
        console.log(err);
        res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}

const deletePhotoById = async (req, res) => {
    const photoid = req.params.photoid;
    if (!mongoose.Types.ObjectId.isValid(photoid)) {
        res.status(400).json({
            message: "Ma photo id khong hop le"
        })
        return false;
    }

    try {
        const result = await photoModel.findByIdAndDelete(photoid);
        if (result) {
            res.status(200).json({
                message: "Xoa thong tin thanh cong",
            })
        }
        else {
            res.status(400).json({
                message: "Khong tim thay thong tin photo",
            })
        }
    } catch (err) {
        console.log(err);
        res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}
const getPhotosOfAlbum= async (req,res)=>{
    const abumId=req.params.abumId;
    const condition={};
    if(mongoose.Types.ObjectId.isValid(abumId))
    {
        condition.abumId=abumId;
    }
  
   
    try{
        const result=await photoModel.find(condition);
        res.status(200).json({
            message:"Lay thong tin thanh cong",
            data:result
        })
    }
    catch(err)
    {
        res.status(500).json({
            message:"Co loi xay ra"
        })
    }
}
module.exports = {
    createPhoto,
    getAllPhoto,
    getPhotoById,
    updatePhotoById,
    deletePhotoById,
    getPhotosOfAlbum
}
