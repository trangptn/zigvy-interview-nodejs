const mongoose = require('mongoose');

const abumModel = require("../models/abumModel");

const createAbum = async (req, res) => {
    //Thu thap du lieu
    const {
        reqUserId,
        reqTitle,
    } = req.body

    //validate du lieu
    if (!mongoose.Types.ObjectId.isValid(reqUserId)) {
        res.status(400).json({
            message: "User Id khong hop le"
        })
        return false;
    }
    if (!reqTitle) {
        res.status(400).json({
            message: "Title khong hop le"
        })
        return false;
    }

    try {
        //Xu ly du lieu
        const newAbum = {
            title: reqTitle,
            userId: reqUserId,
        }
        const result = await abumModel.create(newAbum);
        res.status(201).json({
            message: "Tao moi thanh cong",
            data: result
        })
    } catch (err) {
        console.log(err);
        res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}

const getAllAbum= async (req, res) => {
    const userId=req.query.userId;
    const condition={};
    if(mongoose.Types.ObjectId.isValid(userId))
    {
        condition.userId=userId;
    }

    try {
        const result = await abumModel.find(condition);
        res.status(200).json({
            message: "Lay du lieu thanh cong",
            data: result
        })
    }
    catch (err) {
        res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}

const getAbumById = async (req, res) => {
    const abumid = req.params.abumid;
    if (!mongoose.Types.ObjectId.isValid(abumid)) {
        res.status(400).json({
            message: "Abum Id khong hop le"
        })
        return false;
    }
    const result = await abumModel.findById(abumid);
    try {
        res.status(200).json({
            message: "Lay du lieu thanh cong",
            data: result
        })
    }
    catch (err) {
        res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}

const updateAmbumById = async (req, res) => {
    const abumid = req.params.abumid;
    const {
        reqTitle,
    } = req.body
    //validate du lieu
    if (!mongoose.Types.ObjectId.isValid(abumid)) {
        res.status(400).json({
            message: "Ma abum id khong hop le"
        })
        return false;
    }
    if (reqTitle === "") {
        res.status(400).json({
            message: "Title khong hop le"
        })
        return false;
    }
    try {
        var newAbum= {
        };

        if (reqTitle) {
            newAbum.title = reqTitle;
        }

        const result = await abumModel.findByIdAndUpdate(abumid, newAbum);
        if (result) {
            res.status(200).json({
                message: "Cap nhat thong tin thanh cong",
                data: result
            })
        }
        else {
            res.status(400).json({
                message: "Khong tim thay thong tin abum",
            })
        }
    } catch (err) {
        console.log(err);
        res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}

const deleteAbumById = async (req, res) => {
    const abumid = req.params.abumid;
    if (!mongoose.Types.ObjectId.isValid(abumid)) {
        res.status(400).json({
            message: "Ma abum id khong hop le"
        })
        return false;
    }

    try {
        const result = await abumModel.findByIdAndDelete(abumid);
        if (result) {
            res.status(200).json({
                message: "Xoa thong tin thanh cong",
            })
        }
        else {
            res.status(400).json({
                message: "Khong tim thay thong tin abum",
            })
        }
    } catch (err) {
        console.log(err);
        res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}
const getAlbumsOfUser= async (req,res)=>{
    const userId=req.params.userId;
    const condition={};
    if(mongoose.Types.ObjectId.isValid(userId))
    {
        condition.userId=userId;
    }
    try{
        const result=await abumModel.find(condition);
        res.status(200).json({
            message:"Lay thong tin thanh cong",
            data:result
        })
    }
    catch(err)
    {
        res.status(500).json({
            message:"Co loi xay ra"
        })
    }
}
module.exports = {
    createAbum,
    getAllAbum,
    getAbumById,
    updateAmbumById,
    deleteAbumById,
    getAlbumsOfUser
}
