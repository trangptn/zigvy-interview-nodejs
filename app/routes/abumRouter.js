const express= require('express');

const router=express.Router();

const {
    createAbum,
    getAllAbum,
    getAbumById,
    updateAmbumById,
    deleteAbumById,
    getAlbumsOfUser
}=require("../controllers/abumController");

router.post("/albums",createAbum);
router.get("/albums",getAllAbum);
router.get("/albums/:abumid",getAbumById);
router.put("/albums/:abumid",updateAmbumById);
router.delete("/albums/:abumid",deleteAbumById);
router.get("/users/:userId/albums",getAlbumsOfUser);
module.exports=router;