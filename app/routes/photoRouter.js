const express= require('express');

const router=express.Router();

const {
    createPhoto,
    getAllPhoto,
    getPhotoById,
    updatePhotoById,
    deletePhotoById,
    getPhotosOfAlbum
}=require("../controllers/photoController");

router.post("/photos",createPhoto);
router.get("/photos",getAllPhoto);
router.get("/photos/:photoid",getPhotoById);
router.put("/photos/:photoid",updatePhotoById);
router.delete("/photos/:photoid",deletePhotoById);
router.get("/albums/:abumId/photos",getPhotosOfAlbum);
module.exports=router;