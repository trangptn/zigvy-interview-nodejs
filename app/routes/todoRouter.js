const express= require('express');

const router=express.Router();

const {
    createTodo,
    getAllTodo,
    getTodoById,
    updateTodoById,
    deleteTodoById,
    getTodosOfUser
}=require("../controllers/todoController");

router.post("/todos",createTodo);
router.get("/todos",getAllTodo);
router.get("/todos/:todoid",getTodoById);
router.put("/todos/:todoid",updateTodoById);
router.delete("/todos/:todoid",deleteTodoById);
router.get("/users/:userId/todos",getTodosOfUser);
module.exports=router;