const express= require('express');

const router=express.Router();

const {
    createUser,
    getAllUser,
    getUserById,
    updateUserById,
    deleteUserById,
}=require("../controllers/userController");

router.post("/",createUser);
router.get("/",getAllUser);
router.get("/:userid",getUserById);
router.put("/:userid",updateUserById);
router.delete("/:userid",deleteUserById);
module.exports=router;