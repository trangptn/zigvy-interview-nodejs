const express= require('express');

const router=express.Router();

const {
    createPost,
    getAllPost,
    getPostById,
    updatePostById,
    deletePostById,
    getPostsOfUser
}=require("../controllers/postController");

router.post("/posts",createPost);
router.get("/posts",getAllPost);
router.get("/posts/:postid",getPostById);
router.put("/posts/:postid",updatePostById);
router.delete("/posts/:postid",deletePostById);
router.get("/users/:userId/posts",getPostsOfUser);
module.exports=router;