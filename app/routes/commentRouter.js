const express= require('express');

const router=express.Router();

const {
    createComment,
    getAllComment,
    getComentById,
    updateCommentById,
    deleteCommentById,
    getCommentsOfPost
}=require("../controllers/commentController");

router.post("/comments",createComment);
router.get("/comments",getAllComment);
router.get("/comments/:commentid",getComentById);
router.put("/comments/:commentid",updateCommentById);
router.delete("/comments/:commentid",deleteCommentById);
router.get("/posts/:postId/comments",getCommentsOfPost);
module.exports=router;