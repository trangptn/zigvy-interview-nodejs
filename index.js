//B1: Import thư viện express
//import express from express
const express = require('express');

const mongoose = require('mongoose');

//B2: Khởi tạo app express
const app = new express();

//B3: Khai báo cổng để chạy api
const port = 8000;

const photoModel=require('./app/models/photomodel');
const todoModel=require("./app/models/todoModel");

const userRouter=require("./app/routes/userRouter");

const postRouter=require("./app/routes/postRouter");

const commentRouter=require("./app/routes/commentRouter");

const abumRouter=require("./app/routes/abumRouter");

const photoRouter=require("./app/routes/photoRouter");

const todoRouter=require("./app/routes/todoRouter");

//Cấu hình để sử dụng json
app.use(express.json());

// Khai báo kết nối mongoDB qua mongoose
mongoose.connect("mongodb://127.0.0.1:27017/NR3")
    .then(() => {
        console.log("Connect mongoDB Successfully");
    })
    .catch((err) => {
        console.log(err);
    });

// Sử dụng router 
app.use("/users",userRouter);
app.use("/",postRouter);
app.use("/",commentRouter);
app.use("/",abumRouter);
app.use("/",photoRouter);
app.use("/",todoRouter);
//B4: Start app
app.listen(port, () => {
    console.log(`app listening on port ${port}`);
})
